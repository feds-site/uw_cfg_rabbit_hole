<?php

/**
 * @file
 * uw_cfg_rabbit_hole.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_rabbit_hole_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_module_feds_club';
  $strongarm->value = 'rh_node';
  $export['rh_module_feds_club'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_module_homepage_text_feature';
  $strongarm->value = 'rh_node';
  $export['rh_module_homepage_text_feature'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_action_feds_club';
  $strongarm->value = '3';
  $export['rh_node_action_feds_club'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_action_homepage_text_feature';
  $strongarm->value = '0';
  $export['rh_node_action_homepage_text_feature'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_override_feds_club';
  $strongarm->value = 1;
  $export['rh_node_override_feds_club'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_override_homepage_text_feature';
  $strongarm->value = 1;
  $export['rh_node_override_homepage_text_feature'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_feds_club';
  $strongarm->value = '/clubs';
  $export['rh_node_redirect_feds_club'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_homepage_text_feature';
  $strongarm->value = '';
  $export['rh_node_redirect_homepage_text_feature'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_response_feds_club';
  $strongarm->value = '301';
  $export['rh_node_redirect_response_feds_club'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_response_homepage_text_feature';
  $strongarm->value = '301';
  $export['rh_node_redirect_response_homepage_text_feature'] = $strongarm;

  return $export;
}
